#!/bin/bash -i
#  ____                      _            ____              
# |  _ \ ___ _ __ ___   ___ | |_ ___  ___|  _ \ _   _ _ __  
# | |_) / _ \ '_ ` _ \ / _ \| __/ _ \/ __| |_) | | | | '_ \ 
# |  _ <  __/ | | | | | (_) | ||  __/\__ \  _ <| |_| | | | |
# |_| \_\___|_| |_| |_|\___/ \__\___||___/_| \_\\__,_|_| |_|
#                                                           
# Run several scripts on remote servers automatically.
# Copyright (C) 2022  Ad5001
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

echo ""

set +o history

# Cd into the directory of the library
script="$(readlink $0)"
if [ -z "$script" ]; then script="$0"; fi
cd "$(dirname "$script")"
# Importing common functions.
. ./libraries/common.sh

# Set remotes to remote folder to "remotes folder if not set.
if [ -z "$REMOTES" ]; then
    REMOTES="remotes"
fi


for folder in "$REMOTES"/*; do
    host=$(basename "$folder")
    # Check for powerline.
    if [ -z "$(which powerline)" ]; then
        prompt="${L_GREEN_BG}${BLACK_FG} Password for ${RESET_INVERT}${ORANGE_BG}   $host  ${RESET} 🔑 > "
    else
        prompt="${L_GREEN_BG}${BLACK_FG} Password for ${ORANGE_FG}${INVERT}${RESET_INVERT}${ORANGE_BG}${YELLOW_FG}  $host ${GRAY_FG}${INVERT}${RESET_INVERT}${GRAY_BG}${BLACK_FG} 🔑 ${INVERT} ${RESET}"
    fi
    echo -e -n "$prompt"
    read -s pswd
    echo -e "\n"
    dependencies="libraries/common.sh"
    full_script=""
    port="$(cat $folder/port)"
    # Executing all necessary scripts
    if [[ $pswd != "skip" ]]; then
        for script in $folder/*.sh; do
            scriptname=$(basename $script)
            scriptname=${scriptname//_/ }
            scriptname=${scriptname::-3}
            # Get dependencies from file.
            dependencies="$dependencies $(sed -n '2p' $script | grep "# Requires: " | cut -d" " -f3-)" 
            # Create the full script string using the box command to signal the new script
            full_script="${full_script}box 'Executing script $scriptname...'
$(cat "$script" | awk '$1 != "#" && $1 != ""')
"
        done
        # Removing duplicate dependencies
        dependencies="$(echo $dependencies | awk '!a[$0]++')"
        # Finish the script
        full_script="$(cat $dependencies | awk '$1 != "#" && $1 != ""')
$full_script"
        # Save temporary askpass for the password.
        echo -e '#!/bin/bash\necho "'$pswd'"' > tmp.sh
        chmod +x tmp.sh
        export SSH_ASKPASS="$(pwd)/tmp.sh"
        export SSH_ASKPASS_REQUIRE="force"
        # Transmit the script
        echo "$full_script" | ssh -q $host -p $port "tee run.sh" 1>/dev/null
        info "Transmitted script"
        # Run and remove it.
        ssh $host -p $port PASSWORD="$pswd" "bash run.sh && rm run.sh"
        ## Interactive shell does not work.
        #ssh -t $host PASSWORD="$pswd" "bash -i run.sh && rm run.sh"
        rm tmp.sh
    else
        box "Skipping ${host}..."
    fi
done
